# Biomaterials X (digital) fabrication

Presentor: Ena Naito

Presentation about how to make materials using Kombucha.

## links 🥼

[youtube video](https://www.youtube.com/watch?v=kpIWziUdVv0)

[slides](../files/NFB_KombuchaWorkshop_Ena_compressed.pdf)

![Alt text](../images/biobox.jpeg)

![Alt text](../images/bio_present.jpeg)
