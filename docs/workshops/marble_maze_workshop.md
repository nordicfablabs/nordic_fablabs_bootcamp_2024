# Marble Maze Workshop 

Presenter: Lars Beck Johannsen, Fablab Skanderborg

Marble Maze Modules can be combined together to create a long marble maze run.
There are two rules:

1. Chose on of the four holes in the frame as your start, and another of the four holes on the opposite frame as your exit
2. If using glue, only glue on things you can stick to the frame: Matchstick, IcePopsicle sticks or Lego


![Image1](../images/marble/MarbleMazeModules.jpg)
![Image2](../images/marble/IMG_8871.jpg)
![Image3](../images/marble/IMG_8872.jpg)

# Collaborating

I would like to invite you to the fusion360 project. Just send me the mail you use for Fusion360 to **lars.beck.johannsen@skanderborg.dk**
Gleb suggested to make your own copy of the file in the projectfolder **and** save new addons as components in the project folder, this way you can add those to your own design.

the file is still not 100% parametric, I hope it will be in the future. I know that SideBarLock has issues when you change the height of the SideBars.

# Fabrication files

Three SVG-files made with a lasercutting kerf of 0.15mm. There is a test file your can cut first to see if it fits your laser. If you use Lightburn you can adjust the kerf to have a better fit. The other two are the StraightModule and the 90degreeModule. People have suggested a T-frame... so happy modelling peeps :)

3MF file: The "SideBarLockWrench" you can print to easily disassemble the sidebars from fram if needed
	[GoogleDriveLink](https://drive.google.com/drive/folders/1bmST5vdXiFua1sQ7EdyqrxUYdkZ5hXc3?usp=sharing)

