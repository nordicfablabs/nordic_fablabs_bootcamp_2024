# Code-Based CAD/EDA Workflow with OpenSCAD, SvgPcb and GitLabCI

by Krisjanis Rijnieks, Aalto Fablab

In this workshop we will explore a code-based workflow for creating 3D models and matching circuit boards using code-based tools OpenSCAD (https://openscad.org/) and SvgPcb (https://leomcelroy.com/svg-pcb-website/). Designs in both programs are made using code and it is stored as text files which makes it easy to version control with Git. Both OpenSCAD and SvgPcb offer compatibility with conventional manufacturing processes, such as 3D printing, CNC and PCB milling as well as ordering items online. In this workshop we will design and make a thing that will continue to live as an open source Git repository somewhere on the internet.

During the workshop we looked at CAD with OpenSCAD and the possibility to export images and 3D files used its command-line capability. The same command-line capability makes it possible to automate a project using GitLab CI.

- An [example repository](https://gitlab.com/kriwkrow/code-based-cad-workflow) has been created.
- [SvgPcb introduction video](https://dfs2023.aalto.fi/svgpcb-workshop-video.mp4) Leo did at Aalto a few months ago.