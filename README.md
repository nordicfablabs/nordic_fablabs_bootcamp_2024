# Nordic Fab Labs Bootcamp 2024 Documentation Repository

[Documentation Website](https://nordicfablabs.gitlab.io/nordic_fablabs_bootcamp_2024/)  -
[Program Page](https://nfb2024.aalto.fi/)  -
[Nordic Fab Lab Site](https://nordicfablabs.org/bootcamp-2024-finland/)

## Table of Contents

- [Introduction](#introduction)
- [How to Contribute](#how-to-contribute)
  - [For Newcomers](#for-newcomers)
  - [For Regular Users](#for-regular-users)
- [File Guidelines](#file-guidelines)

## Introduction

Welcome to the documentation repository for the Nordic Fab Labs Bootcamp 2024 held in Aalto! This repository serves as a central hub for all documentation related to the event.

## How to Contribute

We welcome contributions from both regular users and newcomers! Follow the guidelines below to contribute effectively.

### For Newcomers

If you're new to contributing, follow these steps to get started:

1. We recommend working in the online editor using Markdown.
2. Everything being documented is in the `docs` directory.
3. Make your changes and commit them with a descriptive message.
4. When creating a new file, name it using the `.md` extension (e.g., `name_example.md`). Avoid using spaces in the name.
5. If you're not comfortable with Git, you can make changes directly in the online editor.


### For Regular Users

1. Clone the repository to your local machine or work in the online editor.
2. Create a new branch for your changes: `git checkout -b feature/your-feature` if working locally.
3. Make your changes and commit them: `git commit -m "Description of your changes"`.
4. Push your changes to the repository: `git push origin feature/your-feature` if working locally.
5. Create a merge request or open an issue to discuss your changes.

## File Guidelines

To maintain a well-organized and efficient repository, please adhere to the following guidelines when adding files:

- **File Size**: Keep images under 1 MB and slide files under 5 megabytes.
- **Compression**: Compress photos and slides before uploading to reduce file size.
- **Avoid Large Files**: Do not upload files larger than 5 megabytes. If large files need to be shared, provide a link to cloud storage such as Google Drive.

Thank you for contributing to the success of the Nordic Fab Labs Bootcamp 2024 documentation!
