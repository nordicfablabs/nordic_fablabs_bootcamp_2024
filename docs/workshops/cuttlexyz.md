# cuttlexyz workshop

Workshopleader: [@forresto](https://forresto.com)

A web-based design tool for digital cutting machines.
Generate custom SVGs in seconds with templates, or use the editor to make your own designs.

* [cuttle.xyz](https://cuttle.xyz/)

* [Instagram @cuttlexyz](https://www.instagram.com/cuttlexyz/)

---

# Nordic Fablab Bootcamp — Papercraft Workshop 2024-01

> 2D design for 3D forms with Cuttle parametric CAD for lasers

# Why parametric?

Cuttle origin

- [https://tinyletter.com/cuttlexyz/letters/cuttle-a-new-cad-tool-for-rapid-prototyping](https://tinyletter.com/cuttlexyz/letters/cuttle-a-new-cad-tool-for-rapid-prototyping)
- 👆 designing a design app to design a thing [https://sunseeds.xyz](https://sunseeds.xyz/)

# Designs and tools to explore

## Hands-on introduction

Design software and CNC tools have a big learning curve. We’ll explore some designs that can be fabricated before a workshop, to invite immediate hands-on exploration. 

Can it scale up to corrugated? Big d20 or other dice? Irregular crystal shapes?

- Explore polyhedra here: [https://polyhedra.tessera.li](https://polyhedra.tessera.li/)

### Part cut files

Source project: [Cardstock 3D Polyhedron Construction Kit – Metric](https://cuttle.xyz/@forresto/Cardstock-3D-Polyhedron-Construction-Kit-Metric-JHHxX5grjXz8)

Cut layout SVGs, sized for A4:

* [3-sided](../files/cuttle-polyhedra-3.svg)
* [4-sided](../files/cuttle-polyhedra-4.svg)
* [5-sided](../files/cuttle-polyhedra-5.svg)
* [6-sided](../files/cuttle-polyhedra-6.svg)

### Workshop photos

![Group photo, making the shapes](../images/cuttle-polyhedra-group.jpg)

After popping out the triangles and trading them around, the participants started exploring how the parts can come together.

![Cardstock polyhedra examples](../images/cuttle-polyhedra.jpg)

Inventive!

# Cuttle Editor intro and Q&A

Interactive [Cuttle introduction](http://cuttle.xyz/intro).

# More project ideas

[Hexaflexagon Printable](https://cuttle.xyz/@cuttle/Hexaflexagon-Printable-4DRSOggTx9fz)

Can choose your own photos & print directly from Cuttle. Cutting and folding by hand.

## CNC optional

[Parametric Pinwheel Paper Purse](https://cuttle.xyz/@forresto/Parametric-Pinwheel-Paper-Purse-3YomdOUfLZoe)

See also “Ori Revo” below. 

[Villarceau Circles: slot-together torus](https://cuttle.xyz/@forresto/Villarceau-Circles-slot-together-torus-iR64PrVTsIu1)

## CNC recommended

[3D Papercraft Letters Generator](https://cuttle.xyz/@forresto/3D-Papercraft-Letters-Generator-5WoTRVRHdpjU)

Any path can generate a “strip” to give it depth.

[kumiko lattice](https://cuttle.xyz/@forresto/kumiko-lattice-2hU5XrIkYRuC)

Could the top have texture of a mountain range? (Would need a deep design dive.)

# Cool Tools

## Unfolder app

[https://www.unfolder.app](https://www.unfolder.app/) 

Papercraft tool; 3D OBJ to flat layout for cut and score.

## Origami Simulator

[https://origamisimulator.org](https://origamisimulator.org/) 

Flat SVG to simulated folding.

[Origami simulator tips](https://cuttle.xyz/@forresto/Origami-simulator-tips-W4lDXuB5m0xh)

“Miura Ori” … helpful to have folding lines scored beforehand.

## ORI-REVO

![ORI-REVO web app for paper folding design](../images/cuttle-ORI-REVO.png)

[https://mitani.cs.tsukuba.ac.jp/ori_revo/](https://mitani.cs.tsukuba.ac.jp/ori_revo/)

# Laser settings for paper

Don’t use probe for focus, as it will push the card stock down, and be out of focus. 

Tuned on Aalto Fablab’s 80W Epilog Fusion Pro. 

Speed / Power / Frequency. 

## 300gsm cardstock

### Fully cut

85% / 35% / 15%

Smaller parts will fall out. Keep an eye on the progress to be sure that the small parts don’t block later cuts.

### Perforated

85% / 35% / 10%

For holding parts in a sheet “flat-pack.” This is nice for removing keeping things organized, removing the parts from the laser in one sheet, and making kits to pack and ship. 

Changing the frequency is the key to this. Acceleration interacts with this, so straighter segments will have fewer pulses per mm and therefore will have more attachment, compared to shorter segments in the same path. 

Removing the parts from the sheet should be easy by folding along the path a bit. 

![close-up of cardstock laser, cut vs perforated](../images/cuttle-cardstock-laser.png)

### Score, for folding lines

85% / 20% / 5%

Recommended to use “dashed lines” in design, to have half of the path unscored. Might be able to use less power.

### Mark

Needs testing to see if laser can fire with low enough power to only mark card stock. Will probably function as a score line.

## Normal paper

### Score

100% speed, 3% power, 50% frequency

For Jonas's [[Miura Ori]] workshop.

# Materials

I've been using this German "perfect ideaz" brand 300gsm cardstock.

- [Black](https://www.amazon.de/-/en/perfect-ideaz-Sheets-Photo-Germany/dp/B07PKH29BJ/ref=sr_1_5?crid=3U6LX22NMXG53&keywords=300gsm+cardstock&qid=1702988454&sprefix=300gsm+cardstock%2Caps%2C107&sr=8-5)
- [White](https://www.amazon.de/-/en/perfect-ideaz-Sheets-Germany-Certified/dp/B07PH9BZQX/ref=sr_1_6?crid=3U6LX22NMXG53&keywords=300gsm+cardstock&qid=1702990451&sprefix=300gsm+cardstock%2Caps%2C107&sr=8-6)
- [Colors](https://www.amazon.de/perfect-ideaz-Sheets-Coloured-Stock/dp/B079SLF3H1?ref_=ast_sto_dp)

I have also used 300gsm watercolor paper from the art supply store with the same settings and results. 

![forresto's sierpinski lamp making](../images/cuttle-forresto-sierp-lamp-making.jpg)

![forresto's sierpinski lamp](../images/cuttle-forresto-sierp-lamp.jpg)
