# 3D scanning and model workshop

presenter: Solomon

Voxel Kinematics: Combining Motion-Capture, Dance and 3D Scanning

We will explore the possibilities of 3D scanning with the scanners available at the Aalto Fablab and extended capabilities of Aalto Studios motion capture tools. The goal of the workshop is to 3D scan every participant and produce a video where all of us are dancing in sync. This workshop will include a tour to the Aalto Studios mo-cap facility.


Also presented: [art+](https://artplus.app/)

![Alt text](../images/3d_scan.jpeg)