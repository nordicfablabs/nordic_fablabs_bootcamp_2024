# Nordic Fablabs BOOTCAMP documentation SITE

Here you can find documentation gathering for the Nordic Bootcamp 2024 in Aalto.

![group](https://nfb2024.aalto.fi/NFB2024_IMAGE_1920x1080.jpg)

## MEDIA
This are the photos and videos made during the bootcamp by diferent participants. If you wan to add any repository, drive or folder of your photos, just contact Þórarinn, Kris or Árni and/or send the link on discord or whatsapp:

[Frosti's photos](https://drive.google.com/drive/folders/1gJd4rSf5N-XSYGWg93FevwGD37uADnOu?usp=drive_link)

## GENERAL INFORMATION

📍 [Aalto Fablab](https://maps.app.goo.gl/Vpg2Qso2sRnk6MrS9)
🛸 [Program and travel info site](https://nfb2024.aalto.fi)
🛷 [Nordic Fablabs website](https://nordicfablabs.org/)
💾 [Aalto Fablab](https://fablab.aalto.fi/)
🎬 [Aalto Studios](https://studios.aalto.fi/)

![group_2024](images/group_2024.jpeg)


## PROJECTS TO COLABORATE

On the left you have links to the projects and slides of the presentations in the Bootcamp 2024

## social media pages of labs

### instagram 📸

[Tampere](https://www.instagram.com/fablab_tampere_university/) 

[Ísafjöðrur](https://www.instagram.com/fablabisa/)

[Akureyri](https://www.instagram.com/fablabak/)

[Fab Lab Oulu](https://www.instagram.com/fablaboulu)

[Aalto Fablab](https://www.instagram.com/aaltofablab)




